# Todo List and Todo Items

This is a basic todo list block with support for adding todo list items as inner blocks.

# Usage
1. Open a page/post
2. Go to block inserter and insert a Todo list block
3. Add todo list items child blocks inside the todo list block
