import React from 'react';
import renderer from 'react-test-renderer';
import Save from '../save';

describe('Save', () => {
  it('renders correctly', () => {
    const attributes = {
      title: 'My task',
      completed: false,
      textAlign: 'left',
    };
    const tree = renderer.create(<Save attributes={attributes} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
