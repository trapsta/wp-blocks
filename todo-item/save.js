/**
 * WordPress dependencies
 */
import { InnerBlocks, RichText, useBlockProps } from '@wordpress/block-editor';

export default function save( { attributes } ) {
	const { title, completed, textAlign } = attributes;

	const completedClassName = completed ? 'xwp-block-todo-list__task xwp-block-todo-list__task-completed' : 'xwp-block-todo-list__task';
	const alignClassName = textAlign ? `align-${ textAlign }` : '';
	const className = `${ completedClassName } ${ alignClassName }`;

	const blockProps = useBlockProps.save( {
		className,
	} );

	return (
		<li { ...blockProps }>
			<RichText.Content value={ title } />
			<InnerBlocks.Content />
		</li>
	);
}
