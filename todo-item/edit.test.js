import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Edit from './edit';

describe( 'Todo item block edit', () => {
  let setAttributes, attributes;

  beforeEach( () => {
    setAttributes = jest.fn();
    attributes = {
      title: '',
      textAlign: '',
      completed: false,
    };
  } );

  it( 'should render the input form when title is empty', () => {
    const { getByPlaceholderText, getByLabelText } = render(
      <Edit attributes={ attributes } setAttributes={ setAttributes } />
    );
    expect( getByPlaceholderText( 'Add a new task' ) ).toBeInTheDocument();
    expect( getByLabelText( 'Add task' ) ).toBeInTheDocument();
  } );

  it( 'should render the todo item text and toggle button when title is not empty', () => {
    attributes = {
      title: 'Test',
      textAlign: '',
      completed: false,
    };
    const { getByText, getByRole } = render(
      <Edit attributes={ attributes } setAttributes={ setAttributes } />
    );
    expect( getByText( 'Test' ) ).toBeInTheDocument();
    expect( getByRole( 'button', { name: 'Toggle todo list item' } ) ).toBeInTheDocument();
  } );

  it( 'should update the title when the input value changes', () => {
    const { getByPlaceholderText } = render(
      <Edit attributes={ attributes } setAttributes={ setAttributes } />
    );
    const input = getByPlaceholderText( 'Add a new task' );
    fireEvent.change( input, { target: { value: 'Test' } } );
    expect( setAttributes ).toHaveBeenCalledWith( expect.objectContaining( { title: 'Test' } ) );
  } );

  it( 'should call onSubmit when the add task button is clicked', () => {
    const { getByLabelText, getByRole } = render(
      <Edit attributes={ attributes } setAttributes={ setAttributes } />
    );
    const input = getByLabelText( 'Add task' );
    const button = getByRole( 'button', { name: 'Add task' } );
    fireEvent.change( input, { target: { value: 'Test' } } );
    fireEvent.click( button );
    expect( setAttributes ).toHaveBeenCalledWith( expect.objectContaining( { title: 'Test' } ) );
  } );

  it( 'should call toggleTodo when the toggle button is clicked', () => {
    attributes = {
      title: 'Test',
      textAlign: '',
      completed: false,
    };
    const { getByRole } = render(
      <Edit attributes={ attributes } setAttributes={ setAttributes } />
    );
    const button = getByRole( 'button', { name: 'Toggle todo list item' } );
    fireEvent.click( button );
    expect( setAttributes ).toHaveBeenCalledWith( expect.objectContaining( { completed: true } ) );
  } );

  it( 'should call updateTodo when the todo item text is changed', () => {
    attributes = {
      title: 'Test',
      textAlign: '',
      completed: false,
    };
    const { getByText } = render(
      <Edit attributes={ attributes } setAttributes={ setAttributes } />
    );
    const text = getByText( 'Test' );
    fireEvent.change( text, { target: { value: 'Updated Test' } } );
    expect( setAttributes ).toHaveBeenCalledWith( expect.objectContaining( { title: 'Updated Test' } ) );
  } );
} );
