import { __ } from '@wordpress/i18n';
import {
	useBlockProps,
	BlockIcon,
	BlockControls,
	RichText,
	AlignmentControl,
} from '@wordpress/block-editor';
import { Button, ToolbarGroup, ToolbarButton } from '@wordpress/components';
import { plus, trash, check } from '@wordpress/icons';

export default function Edit( { attributes, setAttributes } ) {
	const { title, textAlign, completed } = attributes;

	const onSubmit = ( event ) => {
		event.preventDefault();
		const { value } = event.target.elements.todo;

		if ( ! value ) {
			return;
		}

		const timestamp = new Date().getTime();

		setAttributes( { added: timestamp, title: value, completed: false } );

		event.target.elements.todo.value = '';
	};

	const toggleTodo = () => {
		setAttributes( { completed: ! completed } );
	};

	const updateTodo = ( nextTitle ) => {
		setAttributes( { title: nextTitle } );
	};

	const blockControls = (
		<BlockControls group="block">
			<ToolbarGroup>
				<ToolbarButton
					title={ __( 'Toggle todo list item' ) }
					icon={ check }
					onClick={ () => {
						setAttributes( {
							completed: ! completed,
						} );
					} }
					isPressed={ completed }
					className={
						completed
							? 'xwp-blocks-todo-list-item__completed'
							: 'xwp-blocks-todo-list-item'
					}
					showTooltip
				/>
			</ToolbarGroup>
			<AlignmentControl
				value={ textAlign }
				onChange={ ( newAlign ) =>
					setAttributes( { textAlign: newAlign } )
				}
			/>
		</BlockControls>
	);

	const className = ! title
		? 'xwp-block-todo-list__task-input-form-wrapper'
		: 'xwp-block-todo-list__task';

	const blockProps = useBlockProps( {
		className,
	} );

	return (
		<>
			<li { ...blockProps }>
				{ ! title && (
					<form
						className="xwp-block-todo-list__task-input-form"
						onSubmit={ onSubmit }
					>
						<input
							type="text"
							name="todo"
							placeholder={ __( 'Add a new task', 'xwp-blocks' ) }
							className="xwp-block-todo-list__task-input-field"
						/>
						<Button
							variant="primary"
							className="xwp-block-todo-list__task-submit-button"
							icon={ <BlockIcon icon={ plus } /> }
							type="submit"
							label={ __( 'Add task', 'xwp-blocks' ) }
						/>
					</form>
				) }

				{ title && (
					<>
						<RichText
							value={ title }
							onChange={ updateTodo }
							aria-label={ __( 'Todo item text' ) }
							style={ {
								textDecoration: completed
									? 'line-through'
									: 'none',
								textAlign,
							} }
						/>
						<button
							onClick={ toggleTodo }
							className="xwp-block-todo-list__task-controls"
							style={ {
								background: ! completed ? '#e74c3c' : '#5cb85c',
							} }
						>
							<BlockIcon icon={ trash } />
						</button>
					</>
				) }
			</li>
			{ blockControls }
		</>
	);
}
