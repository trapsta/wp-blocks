import { __, sprintf } from '@wordpress/i18n';
import {
	store as blockEditorStore,
	useBlockProps,
	useInnerBlocksProps,
	BlockControls,
	RichText,
} from '@wordpress/block-editor';
import { Button, ToolbarGroup, ToolbarButton } from '@wordpress/components';
import { heading } from '@wordpress/icons';
import { useSelect, useDispatch } from '@wordpress/data';
import { useEffect } from '@wordpress/element';

const ALLOWED_BLOCKS = [ 'xwpblocks/todo-list-item' ];

export default function Edit( { attributes, setAttributes, clientId } ) {
	const { showLabel, label, todos } = attributes;
	const { updateBlockAttributes } = useDispatch( blockEditorStore );

	const controls = (
		<>
			<BlockControls>
				<ToolbarGroup>
					<ToolbarButton
						title={ __( 'Toggle todo list label' ) }
						icon={ heading }
						onClick={ () => {
							setAttributes( {
								showLabel: ! showLabel,
							} );
						} }
						isPressed={ showLabel }
						className={
							showLabel
								? 'xwp-blocks-todo-lists__control-label'
								: 'xwp-blocks-todo-lists__control'
						}
						showTooltip
					/>
				</ToolbarGroup>
			</BlockControls>
		</>
	);

	const blockProps = useBlockProps( {
		className: 'xwp-block-todo-list',
	} );

	const innerBlocksProps = useInnerBlocksProps( blockProps, {
		allowedBlocks: ALLOWED_BLOCKS,
		__experimentalCaptureToolbars: true,
	} );

	const innerBlockTodos = useSelect(
		( select ) => {
			return select( blockEditorStore ).getBlock( clientId )?.innerBlocks;
		},
		[ clientId ]
	);

	const onClearTodos = () => {
		innerBlockTodos.forEach( ( block ) => {
			updateBlockAttributes( block.clientId, { completed: true } );
		} );
	};

	const pendingTodos =
		innerBlockTodos &&
		innerBlockTodos.filter( ( block ) => {
			return ! block.attributes.completed;
		} );

	useEffect( () => {
		setAttributes( { todos: pendingTodos.length } );
	}, [ pendingTodos ] );

	return (
		<div { ...blockProps }>
			{ controls }

			{ showLabel && (
				<RichText
					className="xwp-block-todo-list__label"
					aria-label={ __( 'Label text', 'xwp-blocks' ) }
					placeholder={ __( 'Add label…', 'xwp-blocks' ) }
					value={ label }
					onChange={ ( html ) => setAttributes( { label: html } ) }
				/>
			) }

			<ul { ...innerBlocksProps } />

			<div className="xwp-block-todo-list__footer">
				<span className="xwp-block-todo-list__footer-label">
					{ sprintf(
						// translators: %d: no of todo items e.g: 1, 4, 10
						__( 'You have %d pending tasks.', 'xwp-blocks' ),
						todos
					) }
				</span>
				{ todos > 0 && (
					<Button variant="secondary" onClick={ onClearTodos }>
						{ __( 'Clear All', 'xwp-blocks' ) }
					</Button>
				) }
			</div>
		</div>
	);
}
