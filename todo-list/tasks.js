export default function Tasks( { todos = [], toggle = () => {}, icon } ) {
	return (
		<ul className="xwp-block-todo-list__tasks">
			{ todos.map( ( todo, index ) => (
				<li
					key={ index }
					className="xwp-block-todo-list__task"
					style={ {
						textDecoration: todo.completed
							? 'line-through'
							: 'none',
					} }
				>
					{ todo.title }
					{ icon && (
						<button
							onClick={ () => toggle( index ) }
							className="xwp-block-todo-list__task-controls"
						>
							{ icon }
						</button>
					) }
				</li>
			) ) }
		</ul>
	);
}
