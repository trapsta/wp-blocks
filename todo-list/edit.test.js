import { render, fireEvent } from '@testing-library/react';
import Edit from './edit';

const mockEditProps = {
	attributes: {
		items: [],
		showLabel: false,
		label: '',
	},
	setAttributes: jest.fn(),
	clientId: 0,
};

beforeEach( () => {
	mockEditProps.setAttributes.mockClear();
} );

it('should toggle the showLabel attribute when the ToolbarButton is clicked', () => {
  const setAttributes = jest.fn();
  const { getByTitle } = render(<Edit {...mockEditProps} />);
  fireEvent.click(getByTitle('Toggle todo list label'));
  expect(setAttributes).toHaveBeenCalledWith({ showLabel: true });
});

it('should call the setAttributes function with the correct label when the RichText is changed', () => {
  const setAttributes = jest.fn();
  const { getByLabelText } = render(<Edit {...mockEditProps}  />);
  fireEvent.change(getByLabelText('Label text'), { target: { value: 'New label' } });
  expect(setAttributes).toHaveBeenCalledWith({ label: 'New label' });
});


it('should set the completed attribute of each todo item to true when the Clear All button is clicked', () => {
	const updateBlockAttributes = jest.fn();
	const innerBlocks = [
	  { clientId: 1, attributes: { completed: false } },
	  { clientId: 2, attributes: { completed: false } },
	  { clientId: 3, attributes: { completed: true } },
	];
	const { getByText } = render(<Edit {...mockEditProps} innerBlocks={innerBlocks}  attributes={{}} updateBlockAttributes={updateBlockAttributes} />);
	fireEvent.click(getByText('Clear All'));
	expect(updateBlockAttributes).toHaveBeenCalledTimes(2);
	expect(updateBlockAttributes).toHaveBeenCalledWith(1, { completed: true });
	expect(updateBlockAttributes).toHaveBeenCalledWith(2, { completed: true });
  });

  it('should update the todos attribute when the number of pending todos changes', () => {
	const setAttributes = jest.fn();
	const innerBlocks = [
	  { clientId: 1, attributes: { completed: false } },
	  { clientId: 2, attributes: { completed: false } },
	  { clientId: 3, attributes: { completed: true } },
	];
	render(<Edit {...mockEditProps} innerBlocks={innerBlocks}  attributes={{}} setAttributes={setAttributes} clientId={0} />);
	expect(setAttributes).toHaveBeenCalledTimes(1);
	expect(setAttributes).toHaveBeenCalledWith({ todos: 2 });
  });


