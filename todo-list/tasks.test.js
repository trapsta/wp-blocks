import { render, screen, fireEvent } from '@testing-library/react';
import Tasks from './tasks';

describe('Tasks', () => {
  const todos = [
    { title: 'Task 1', completed: false },
    { title: 'Task 2', completed: true },
  ];

  const toggleMock = jest.fn();

  it('should render the list of tasks', () => {
    render(<Tasks todos={todos} />);
    const taskList = screen.getByRole('list', { name: 'List of tasks' });
    expect(taskList).toBeInTheDocument();

    const taskItems = screen.getAllByRole('listitem');
    expect(taskItems.length).toBe(todos.length);

    expect(taskItems[0]).toHaveTextContent(todos[0].title);
    expect(taskItems[0]).not.toHaveStyle('text-decoration: line-through;');

    expect(taskItems[1]).toHaveTextContent(todos[1].title);
    expect(taskItems[1]).toHaveStyle('text-decoration: line-through;');
  });

  it('should call the toggle function when the icon button is clicked', () => {
    render(<Tasks todos={todos} toggle={toggleMock} icon={<span>ICON</span>} />);
    const iconButtons = screen.getAllByRole('button', { name: 'Toggle task completion' });
    fireEvent.click(iconButtons[0]);
    expect(toggleMock).toHaveBeenCalledWith(0);
  });
});
