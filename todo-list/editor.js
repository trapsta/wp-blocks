/**
 * Block type editor script definition.
 * It will only be enqueued in the context of the editor.
 */

/**
 * Internal dependencies
 */
import metadata from './block.json';
import Edit from './edit';
import Save from './save';

const { name } = metadata;

const settings = {
	edit: Edit,
	save: Save,
};

export { name, settings };
