import { render } from '@testing-library/react';
import Save from './save';

describe('TodoList Block - save function', () => {
  it('should match snapshot', () => {
    const attributes = {
      label: 'Todo List',
      showLabel: true,
      todos: [
        {
          title: 'Todo Item 1',
          completed: false,
          textAlign: 'left',
        },
        {
          title: 'Todo Item 2',
          completed: true,
          textAlign: 'center',
        },
      ],
    };
    const { asFragment } = render(<Save attributes={attributes} />);
    expect(asFragment()).toMatchSnapshot();
  });
});

describe('TodoList Block - save function', () => {
  it('should show label if showLabel is true', () => {
    const attributes = {
      label: 'Todo List',
      showLabel: true,
      todos: [],
    };
    const { getByText } = render(<Save attributes={attributes} />);
    expect(getByText(attributes.label)).toBeInTheDocument();
  });

  it('should not show label if showLabel is false', () => {
    const attributes = {
      label: 'Todo List',
      showLabel: false,
      todos: [],
    };
    const { queryByText } = render(<Save attributes={attributes} />);
    expect(queryByText(attributes.label)).not.toBeInTheDocument();
  });

  it('should show "No todos yet." message if there are no todos', () => {
    const attributes = {
      label: 'Todo List',
      showLabel: true,
      todos: [],
    };
    const { getByText } = render(<Save attributes={attributes} />);
    expect(getByText('No todos yet.')).toBeInTheDocument();
  });

  it('should show the correct number of pending tasks', () => {
    const attributes = {
      label: 'Todo List',
      showLabel: true,
      todos: [
        {
          title: 'Todo Item 1',
          completed: false,
          textAlign: 'left',
        },
        {
          title: 'Todo Item 2',
          completed: true,
          textAlign: 'center',
        },
        {
          title: 'Todo Item 3',
          completed: false,
          textAlign: 'right',
        },
      ],
    };
    const { getByText } = render(<Save attributes={attributes} />);
    const pendingTasks = attributes.todos.filter((todo) => !todo.completed);
    const expectedText = `You have ${pendingTasks.length} pending tasks.`;
    expect(getByText(expectedText)).toBeInTheDocument();
  });
});
