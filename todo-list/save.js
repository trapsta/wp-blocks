import { sprintf, __ } from '@wordpress/i18n';
import {
	useBlockProps,
	useInnerBlocksProps,
	RichText,
} from '@wordpress/block-editor';

export default function save( { attributes } ) {
	const { label, showLabel, todos } = attributes;

	const className = 'xwp-block-todo-list';
	const blockProps = useBlockProps.save( { className } );
	const innerBlocksProps = useInnerBlocksProps.save( blockProps );

	return (
		<div { ...blockProps }>
			{ showLabel && (
				<RichText.Content
					className="xwp-block-todo-list__label"
					aria-label={ __( 'Label text', 'xwp-blocks' ) }
					value={ label }
				/>
			) }

			{ todos.length < 1 && (
				<>
					<p>✌️</p>
					<p>{ __( 'No todos yet.', 'xwp-blocks' ) }</p>
				</>
			) }

			<ul { ...innerBlocksProps } />

			<div className="xwp-block-todo-list__footer">
				<span className="xwp-block-todo-list__footer-label">
					{ sprintf(
						// translators: %d: no of todo items e.g: 1, 4, 10
						__( 'You have %d pending tasks.', 'xwp-blocks' ),
						todos
					) }
				</span>
			</div>
		</div>
	);
}
